from django.conf.urls import url

from . import views

app_name = 'content'

urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^(?P<page_number>[0-9]+)/', views.IndexView.as_view(), name='index_page'),
    url(r'task/(?P<pk>[0-9]+)/', views.TaskView.as_view(), name='task')
]
