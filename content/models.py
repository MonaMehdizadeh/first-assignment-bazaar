from django.db import models

class Users(models.Model):
    name = models.CharField(max_length=200)
    email = models.EmailField()
    phone_number = models.CharField(max_length=20)
    master = models.BooleanField()
    
    def __str__(self):
        return self.name

class Tasks(models.Model):
    task_name = models.CharField(max_length=200)
    estimated_time = models.DurationField()
    value = models.DecimalField(max_digits=10, decimal_places=2)
    deadline = models.DateTimeField()
    master = models.ForeignKey('Users', on_delete=models.CASCADE, 
                                related_name = "master_user")
    slave = models.ForeignKey('Users', on_delete=models.CASCADE, 
                                related_name = "slave_user")
    pub_date = models.DateTimeField()
    description = models.TextField(default="No description.")

    def __str__(self):
        return self.task_name
