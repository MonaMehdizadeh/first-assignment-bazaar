from django.shortcuts import render, get_object_or_404
from django.template import loader
from django.http import HttpResponseRedirect, HttpResponse
from django.http import Http404
from django.urls import reverse
from django.views import generic
from django.utils import timezone
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from .models import Users,Tasks

class IndexView(generic.ListView):
    template_name = 'content/index.html'
    context_object_name = 'tasks_list'
    
    def __init__(self):
        self.page_size = 1

    def dispatch(self, request, *args, **kwargs):
        page_number = kwargs.get('page_number', None)
        if page_number is None:
            self.page_number = 1
        else:
            self.page_number = page_number
        try:
            ret = super(IndexView, self).dispatch(request, *args, **kwargs)
        except AttributeError:
            raise Http404
        return ret

    def get_queryset(self):
        """Return the tasks list."""
        tasks = Tasks.objects.all()
        paginator = Paginator(tasks, 1)
        try:
            queryset = paginator.page(self.page_number)
        except PageNotAnInteger:
        # If page is not an integer, deliver first page.
            queryset = paginator.page(1)
        except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
            queryset = paginator.page(paginator.num_pages)
        return queryset

class TaskView(generic.DetailView):
    model = Tasks
    template_name = 'content/task.html'





