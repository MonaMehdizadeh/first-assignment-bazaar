from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'task/(?P<task_number>[0-9]+)/details/$', views.tasks, name="tasks"),
]

